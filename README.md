# Fractal Generator


## Comment lancer le programme :
Pour lancer le programme, appuyer sur « Run ». 
Si toutes les bibliothèques sont installées, le programme devrait se lancer et l’interface s’afficher après quelques secondes.

## Comment accéder aux différentes fonctionnalités :
### 1. Modifier le type de fractale

Appuyer sur le bouton radio voulu (Mandelbrot ou Julia) et appuyer sur le bouton « Générer » correspondant. Veillez à bien avoir sélectionné le bon bouton radio avant de générer, auquel cas la génération ne se réalisera pas.
La génération peut prendre plusieurs dizaines de secondes (surtout pour l’ensemble de Julia). Pour savoir si la page est bien en train de générer la fractale, regarder la barre de chargement en bas à gauche. Si elle est arrêtée, c’est que la page est bien en train de générer (dans ce cas, ne pas appuyer sur quoi que ce soit et attendre le nouvel affichage) ; mais si elle continue d’aller de gauche à droite, c’est que la page n’est pas en train de générer (voir dans ce cas si le bouton radio a bien été sélectionné). 
Pendant la génération, attendre quelques secondes même si la page indique « ne répond pas » (la génération de la fractale se réalise en réalité).

### 2. Modifier le nombre d’itérations
Faire glisser le curseur du nombre d’itérations (allant de 0 à 160) au niveau souhaité. Appuyer ensuite sur « Générer » pour voir les modifications. La génération peut là aussi prendre quelques secondes.

### 3. Modifier la formule de Julia
La modification de la formule de Julia peut se réaliser de deux façons.
L’utilisateur peut d’abord saisir sa propre formule de Julia dans l’Entry en suivant le modèle « x + y j » pour x et y allant d’environ -0.685 à 0.685, et appuyer sur « Générer ».
L’utilisateur peut aussi choisir parmi les 6 valeurs remarquables que nous avons proposées. Leur nom ne leur à pas été donné officiellement, c’est nous qui les avons nommées de par ce qu’elles représentent à l’image. L’utilisateur peut donc sélectionner l’un des six boutons, ce qui modifiera directement la formule dans l’Entry avec la formule correspondante au bouton. A partir de là, il suffit d’appuyer sur « Générer » (noter que la génération d’un ensemble de Julia peut prendre plusieurs dizaines de secondes, voire plusieurs minutes si l’option de qualité choisie est « Haute »).

### 4. Modifier la palette de couleur utilisée
Choisir la palette de couleur voulue parmi celles proposées dans la « Combobox » (menu déroulant) et appuyer sur le bouton « Modifier » pour mettre à jour les couleurs instantanément.

### 5. Zoomer sur la fractale
Pour zoomer sur la fractale et voir tous ses détails, l’utilisateur peut appuyer sur la Loupe tout en bas à gauche de l’écran (à côté de la Maison permettant de revenir à la vue initiale et des Réglages permettant de modifier sensiblement l’emplacement de l’image).
Une fois la Loupe sélectionnée, l’utilisateur peut créer une cadre sur l’image à l’aide de sa souris. Ce nouveau cadre constituera la nouvelle image, qui sera alors zoomée.
Pour quitter le mode Loupe, appuyer de nouveau sur cette dernière.

### 6. Changer d’apparence
Pour changer l’apparence de l’interface (et de la fractale en passant), l’utilisateur peut sélectionner le mode qu’il souhaite (« Light » ou « Dark ») dans la Combobox située en bas de la barre latérale.
Le changement d’apparence peut prendre plusieurs secondes puisqu’en plus de modifier l’apparence de l’interface, une nouvelle fractale est générée suivant le mode choisi (c’est une idée que nous avons eu de créer des fractales « claires » ou « sombres », en voyant que le cœur des fractales peut être noir ou à l’inverse blanc en fonction de la logique mathématique utilisée).

### 7. Changer la qualité d’image
Pour changer la qualité d’image de la fractale, l’utilisateur peut sélectionner l’option de qualité qu’il souhaite (« Basse » ou « Haute ») dans la Combobox située tout en bas de la barre latérale.
L’opération prend quelques secondes puisqu’une nouvelle fractale est générée avec l’option de qualité choisie.
(Nous vous déconseillons de générer des Ensembles de Julia en haute qualité, leur génération prenant 6 à 7 min).

### 8. Sauvegarder l’image
Pour sauvegarder l’image, appuyer sur le bouton « Télécharger » en haut de la barre latérale. S’ouvrira alors une fenêtre demandant à l’utilisateur de nommer le fichier .png contenant l’image.

### 9. Quitter
Appuyer sur le bouton « Quitter » ou bien sur la croix rouge en haut à droite de la fenêtre.