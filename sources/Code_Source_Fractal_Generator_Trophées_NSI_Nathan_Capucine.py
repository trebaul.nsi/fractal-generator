# ------- IMPORTATION des bibliothèques nécessaires (à installer avec "pip install .....") -------

# Pour l'interface
from tkinter import *
from tkinter import filedialog
import customtkinter


# Pour le calcul
import numpy as np


# Pour l'affichage de la fractale (Matplotib)
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
import matplotlib.pyplot as plt
from matplotlib import backend_bases




# INITIALISATION de l'interface par défaut
customtkinter.set_appearance_mode("Light")  # Modes: "Dark", "Light"
customtkinter.set_default_color_theme("blue")  # Thèmes: "blue" (standard), "green", "dark-blue"


# INITIALISATION des outils Matplotlib
backend_bases.NavigationToolbar2.toolitems = (
        ('Home', 'Reset original view', 'home', 'home'), # Revenir à l'image de base
        ('Zoom', 'Zoom to rectangle', 'zoom_to_rect', 'zoom'), # Zoom
        ('Subplots', 'Configure subplots', 'subplots', 'configure_subplots'), # Configurer l'emplacement et la taille de l'image
      )





# ------- CLASSE PRINCIPALE -------

class Generateur_fractale:
    def __init__(self, fenetre):

        # Initialisation de la fenêtre CustomTkinter (titre, taille et mode)
        self.fenetre = fenetre
        self.fenetre.title("Fractal Generator")
        self.fenetre.geometry(f"{1365}x{910}")
        self.mode = "Light"

        # Configuration du layout de la grille
        self.fenetre.grid_columnconfigure(1, weight=0)
        self.fenetre.grid_columnconfigure((2, 3), weight=1)
        self.fenetre.grid_rowconfigure((0, 1, 2), weight=1)





        # ------- INITIALISATION des paramètres principaux et de leur valeur PAR DÉFAUT -------

        self.largeur, self.hauteur = 800, 800 # Définit la "qualité" de l'image


        self.x_min, self.x_max = -2, 2
        self.y_min, self.y_max = -2, 2 # Définissent les dimensions des axes


        self.max_iter = 160 # Définit le nombre d'itération de la fractale


        self.type_fractale = "mandelbrot" # Fractale de Mandelbrot par défaut (possibilité de choisir celle de Julia sur l'interface)
        self.type_fractale_var = StringVar()
        self.type_fractale_var.set(self.type_fractale) # Stockage dans une variable pour pouvoir la manipuler


        self.formule_julia = '-0.8+0.156j'  # Formule par défaut pour l'ensemble de Julia
        self.formule_julia_var = StringVar()
        self.formule_julia_var.set(self.formule_julia) # Stockage dans une variable

 
        self.couleur = 'magma' # Palette de couleur par défaut (provient des palettes officiels de Matplotlib)
        self.couleur_var = StringVar()
        self.couleur_var.set(self.couleur) # Stockage dans une variable






        # ------- CRÉATION DES WIDGETS -------


        # --- BARRE LATÉRALE ---

        # Création
        self.barre_laterale = customtkinter.CTkFrame(self.fenetre, width=120, corner_radius=10)
        self.barre_laterale.grid(row=0, column=0, rowspan=4, sticky="nsew", padx=10,pady=10)
        self.barre_laterale.grid_rowconfigure(4, weight=1)


        # Logo principal
        self.logo_label = customtkinter.CTkLabel(self.barre_laterale, text="Fractal Generator", font=customtkinter.CTkFont(size=20, weight="bold"))
        self.logo_label.grid(row=0, column=0, padx=20, pady=(30, 30), ipadx = 20,sticky="nsew")


        # Boutons de Téléchargement de l'image et de Sortie du programme
        telecharger_button = customtkinter.CTkButton(self.barre_laterale, text="Télécharger",
                                              command=self.sauvegarder_fichier) # appel de la fonction "sauvegarder_fichier" pour exporter
        telecharger_button.grid(row=2, column=0, padx=20, pady= (15,5))

        quitter_button = customtkinter.CTkButton(self.barre_laterale, text="Quitter", fg_color="transparent", border_width=2, text_color=("gray10", "#DCE4EE"),
                                              command=self.fenetre.destroy) # appel de "destroy" pour quitter
        quitter_button.grid(row=3, column=0, padx=20, pady=5)


        # Menu d'option d'Apparence (Mode Clair/Sombre)
        self.apparence_mode_label = customtkinter.CTkLabel(self.barre_laterale, text="Thème:", font=customtkinter.CTkFont())
        self.apparence_mode_label.grid(row=5, column=0, padx=20, pady=(0, 7), sticky = "ew")

        self.apparence_mode_optionemenu = customtkinter.CTkOptionMenu(self.barre_laterale, values=["Light", "Dark"],
                                                                       command=self.change_apparence_mode_event) # appel de la fonction pour changer d'apparence
        self.apparence_mode_optionemenu.grid(row=6, column=0, padx=20, pady=(0, 20), sticky = "ew")


        # Menu d'option de Qualité d'image (Faible(=rapide) / Haute(=lente))
        self.qualite_mode_label = customtkinter.CTkLabel(self.barre_laterale, text="Qualité:", font=customtkinter.CTkFont())
        self.qualite_mode_label.grid(row=7, column=0, padx=20, pady=(0, 7), sticky = "ew")

        self.qualite_mode_optionemenu = customtkinter.CTkOptionMenu(self.barre_laterale, values=["Basse (plus rapide)", "Haute (plus lente)"],
                                                                       command=self.change_qualite_mode_event) # appel de fonction pour modifier la qualité
        self.qualite_mode_optionemenu.grid(row=8, column=0, padx=20, pady=(0, 20), sticky = "ew")


        # Progressbar indeterminée : permet de savoir lorsque la page est en chargement (car lorsqu'elle charge, la progressbar s'arrête)
        self.progressbar = customtkinter.CTkProgressBar(self.barre_laterale)
        self.progressbar.grid(row=9, column=0, padx=20, pady=(10, 20), sticky="ew")
        self.progressbar.configure(mode="indeterminnate")
        self.progressbar.start()


        





        # --- MENU MANDELBROT ---

        # Création
        self.mandelbrot_frame = customtkinter.CTkFrame(self.fenetre, width=140, corner_radius=10)
        self.mandelbrot_frame.grid(row=0, column=1, sticky="nsew", padx=10,pady=10)


        # Titre du menu Mandelbrot
        self.titre_mandel = customtkinter.CTkLabel(master=self.mandelbrot_frame, text="Ensemble de Mandelbrot", font=customtkinter.CTkFont(size=15))
        self.titre_mandel.grid(row=0, column=0, columnspan=2, padx=30, pady=20, sticky="w")


        # Bouton radio Mandelbrot pour choisir le type de fractale
        self.radio_button_mandel = customtkinter.CTkRadioButton(master=self.mandelbrot_frame, text="Mandelbrot", variable=self.type_fractale_var, value = "mandelbrot") # on relie la valeur du bouton à la variable StringVar définie l.70
        self.radio_button_mandel.grid(row=1, column=0, columnspan=2, pady=10, padx=30, sticky="nsew")


        # Titre de la rubrique "Nombre d'itérations"
        self.iter_label = customtkinter.CTkLabel(master=self.mandelbrot_frame, text="Nombre d'itérations :", font=customtkinter.CTkFont())
        self.iter_label.grid(row=2, column=0, columnspan=2, padx=50, pady=(20,0), sticky="w")


        # Labels indiquant le nombre d'itérations minimales (0) et maximales (160)
        self.iter_min_label = customtkinter.CTkLabel(master=self.mandelbrot_frame, text="0", font=customtkinter.CTkFont(size=15))
        self.iter_min_label.grid(row=3, column=0, padx=(50,0), pady = (5,10))

        self.iter_max_label = customtkinter.CTkLabel(master=self.mandelbrot_frame, text="160", font=customtkinter.CTkFont(size=15))
        self.iter_max_label.grid(row=3, column=2, columnspan=1, padx=(0,50), sticky="w")


        # Slider permettant directement de modifier le nombre d'itérations
        self.iter_slider = customtkinter.CTkSlider(self.mandelbrot_frame, from_=0, to=160, number_of_steps=30)
        self.iter_slider.set(self.max_iter) # initilisation au nombre d'itération par défaut
        self.iter_slider.grid(row=3, column=1, columnspan=1, padx=5, pady=10, sticky="ew")
  

        # Bouton de mise à jour de la fractale (compter quelques secondes avant de voir apparaitre la modification)
        update_mandelbrot_button = customtkinter.CTkButton(self.mandelbrot_frame, text="Générer", width=90, corner_radius=13, command=self.update_mandelbrot) # appel de la fonction de mise à jour
        update_mandelbrot_button.grid(row=5, column=0, columnspan=3, padx=15, pady = (20,0), sticky = "w")
        
     






        # --- MENU JULIA ---

        # Création
        self.julia_frame = customtkinter.CTkFrame(self.fenetre, width=140, corner_radius=10)
        self.julia_frame.grid(row=1, column=1, sticky="nsew", padx=10,pady=10)


        # Titre du menu Julia
        self.titre_julia = customtkinter.CTkLabel(master=self.julia_frame, text="Ensemble de Julia", font=customtkinter.CTkFont(size=15))
        self.titre_julia.grid(row=0, column=0, columnspan=2, padx=30, pady=20, sticky="w")


        # Bouton radio Julia pour choisir le type de fractale
        self.radio_button_julia = customtkinter.CTkRadioButton(master=self.julia_frame, text="Julia", variable=self.type_fractale_var, value = "julia") # on relie la valeur du bouton à la variable StringVar définie l.70
        self.radio_button_julia.grid(row=1, column=0, columnspan = 2, pady=10, padx=30, sticky="nsew")


        # Label indiquant la formule spécifique entrée par l'utilisateur
        self.formule_label = customtkinter.CTkLabel(master=self.julia_frame, text="Formule de Julia :", font=customtkinter.CTkFont())
        self.formule_label.grid(row=2, column=0, padx=(50,18), pady = (15,10), sticky = "n")


        # Entry permettant à l'utilisateur de saisir sa propre formule
        self.formule_julia_entry = customtkinter.CTkEntry(self.julia_frame, textvariable=self.formule_julia_var) # on relie la valeur de l'entry à la variable StringVar définie l.75
        self.formule_julia_entry.grid(row=2, column=1, padx=(0,10), pady = (15,10))
        self.formule_julia_entry.bind("<Return>", self.update_julia) # génère directement la fractale si l'on appuie sur "Entrée"




        # - FENETRE DES VALEURS REMARQUABLES -

        # Création
        self.val_remarq_frame = customtkinter.CTkFrame(self.julia_frame, height = 70, width = 140)
        self.val_remarq_frame.grid(row=3, column=0, columnspan=2, padx=(28,0), pady=(10,5),sticky = "nsew")
        self.val_remarq_frame.grid_columnconfigure(3)


        # Titre de la rubrique "Valeurs remarquables"
        self.val_remarq_label = customtkinter.CTkLabel(master=self.val_remarq_frame, text="Valeurs remarquables", font=customtkinter.CTkFont())
        self.val_remarq_label.grid(row=0, column=0, columnspan=3, padx=23, pady=10, sticky = "w")


        # Création des 6 boutons permettant de chosir les 6 valeurs remarquables (que nous avons choisies)
        self.val1_button = customtkinter.CTkButton(self.val_remarq_frame, text="Galaxy", width=70, fg_color="transparent", border_width=2, text_color=("gray10", "#DCE4EE"), command = lambda: self.formule_julia_var.set("0.285+0.01j"))
        self.val1_button.grid(row=1, column=0, padx=(17,10), pady=5, sticky = "ew")

        self.val2_button = customtkinter.CTkButton(self.val_remarq_frame, text="Fracture", width=70, fg_color="transparent", border_width=2, text_color=("gray10", "#DCE4EE"), command = lambda: self.formule_julia_var.set("-0.02+0.75j"))
        self.val2_button.grid(row=1, column=1, padx=10, pady=5, sticky = "ew")

        self.val3_button = customtkinter.CTkButton(self.val_remarq_frame, text="Firebolt", width=70, fg_color="transparent", border_width=2, text_color=("gray10", "#DCE4EE"), command = lambda: self.formule_julia_var.set("0.285+0.013j"))
        self.val3_button.grid(row=1, column=2, padx=10, pady=5, sticky = "e")

        self.val4_button = customtkinter.CTkButton(self.val_remarq_frame, text="Flowers", width=70, fg_color="transparent", border_width=2, text_color=("gray10", "#DCE4EE"), command = lambda: self.formule_julia_var.set("-0.4+0.6j"))
        self.val4_button.grid(row=2, column=0, padx=(17,10), pady=(5,10), sticky = "ew")

        self.val5_button = customtkinter.CTkButton(self.val_remarq_frame, text="Royal", width=70, fg_color="transparent", border_width=2, text_color=("gray10", "#DCE4EE"), command = lambda: self.formule_julia_var.set("-0.8+0.156j"))
        self.val5_button.grid(row=2, column=1, padx=10, pady=(5,10), sticky = "ew")

        self.val6_button = customtkinter.CTkButton(self.val_remarq_frame, text="Spiral", width=70, fg_color="transparent", border_width=2, text_color=("gray10", "#DCE4EE"), command = lambda: self.formule_julia_var.set("0.241+0.561j"))
        self.val6_button.grid(row=2, column=2, padx=10, pady=(5,10), sticky = "e")

        

        # Bouton de mise à jour de la fractale avec la formule choisie
        update_julia_button = customtkinter.CTkButton(self.julia_frame, text="Générer", width=90, corner_radius=13, command=self.update_julia) # appel de la fonction de modification de formule et de génération de la fractale
        update_julia_button.grid(row=5, column=0, columnspan=2, padx=15, pady = (20,0), sticky="w")








        # --- MENU COULEURS ---

        # Création
        self.couleur_frame = customtkinter.CTkFrame(self.fenetre, width=140, corner_radius=10)
        self.couleur_frame.grid(row=2, column=1, sticky="nsew", padx=10,pady=10)


        # Titre du menu Couleurs
        self.titre_couleur = customtkinter.CTkLabel(master=self.couleur_frame, text="Couleurs", font=customtkinter.CTkFont(size=15))
        self.titre_couleur.grid(row=0, column=0, columnspan=2, padx=30, pady=(20,15), sticky="w")


        # Label indiquant le choix de la palette
        self.palette_label = customtkinter.CTkLabel(master=self.couleur_frame, text="Choix de la palette :", font=customtkinter.CTkFont())
        self.palette_label.grid(row=1, column=0, padx=(50,5), pady = (5,5), sticky = "ws")


        # Menu déroulant permettant de choisir la palette de couleur parmi celles que nous avons sélectionnées
        couleur_combobox = customtkinter.CTkComboBox(self.couleur_frame, variable=self.couleur_var, # on relie la valeur sélectionnée à la variable StringVar définie l.80
                                                     values=['magma','CMRmap','hot','turbo','terrain','hsv','prism','nipy_spectral','pink','gist_earth','gist_stern','binary_r'])
        couleur_combobox.grid(row=1, column=1, columnspan=2, padx=(15,40), sticky="w")


        # Bouton de mise à jour de la fractale avec la couleur sélectionnée
        update_couleur_bouton = customtkinter.CTkButton(self.couleur_frame, text="Modifier", width=90, corner_radius=13, command=self.update_couleur) # appel de la fonction de changement de couleur
        update_couleur_bouton.grid(row=2, column=0, columnspan=2, padx=15, pady = (20,0), sticky = "w")








        # --- ESPACE AFFICHAGE MATPLOTLIB ---

        # Création du "plot" (là où l'affichage est réalisé)
        self.figure, self.axes = plt.subplots()


        # Couleur du background est la même que celle de l'interface CustomTkinter
        self.figure.set_facecolor("#ebebeb")


        # Création d'un canvas spécial (FigureCanvasTkAgg) sur lequel un plot Matplotlib peut être inséré
        self.canvas = FigureCanvasTkAgg(self.figure, master=self.fenetre)


        # Positonnement du canvas spécial sur la fenêtre grâce à self.canvas_widget              
        self.canvas_widget = self.canvas.get_tk_widget()
        self.canvas_widget.grid(row=0,column=2,rowspan=4,columnspan=4,sticky="nsew")
        self.canvas_widget.grid_columnconfigure(2,weight=1)
     

        # Création et positionnement de la barre d'outil Matplotlib contenant les outils définis l.28
        self.barre_outils = NavigationToolbar2Tk(self.canvas, self.fenetre, pack_toolbar=False)
        self.barre_outils.grid(columnspan=4, sticky="nsew")


        # Initialisation d'un rendu image avec "self.afficher_fractale()""
        self.rendu_image = None
        self.afficher_fractale()



        # - Mainloop -
        self.fenetre.mainloop()









    # ------- MÉTHODES (FONCTIONNALITÉS) -------


    # 1. Sauvegarder l'image
    def sauvegarder_fichier(self):
        '''Sauvegarde de l'image dans un fichier .png'''

        # on enlève temporairement le barre de couleur pour n'avoir que la fractale elle-même en photo
        self.barre_de_couleur.remove()

        # on enregistre l'image dans un fichier .png dont le nom est saisi par l'utilisateur
        self.figure.savefig(filedialog.asksaveasfilename(defaultextension='.png',title = "Name file",filetypes = (('PNG', '*.png'), ('JPEG', ('*.jpg','*.jpeg','*.jpe','*.jfif')))), transparent = True)

        # on remet la barre de couleur après l'opération
        self.barre_de_couleur = self.figure.colorbar(self.rendu_image)






    # 2. Changer l'apparence (mode clair / mode sombre)
    def change_apparence_mode_event(self, nouvelle_apparence: str):
        '''Appelle respectivement les méthodes "mode_clair" ou "mode_sombre" en fonction de l'apparence sélectionée
           Entrée : valeur sélectionnée dans le menu d'apparence'''

        if nouvelle_apparence == "Light":
            self.mode_clair()
        else:
            self.mode_sombre()




    # 2.a : Mode clair
    def mode_clair(self):
        '''Sous-méthode appelée par "change_apparence_mode_event" si l'apparence sélectionée est "Light"'''

        # mise à jour de l'attribut
        self.mode = "Light"

        # affichage de la nouvelle fractale en mode "clair"
        self.afficher_fractale()

        # Modification de toute l'interface (CustomTkinter, figure Matplotlib et barre d'outils)
        customtkinter.set_appearance_mode("Light")

        self.figure.set_facecolor("#ebebeb") # "ebebebe" est le code couleur correspondant à l'interface claire
        
        self.barre_outils.config(background="#ebebeb")
        self.barre_outils._message_label.config(background="#ebebeb")
        for bouton in self.barre_outils.winfo_children():
            bouton.config(background="#ebebeb")
        self.barre_outils.update()




    # 2.b : Mode sombre
    def mode_sombre(self):
        '''Sous-méthode appelée par "change_apparence_mode_event" si l'apparence sélectionée est "Dark"'''

        # mise à jour de l'attribut
        self.mode = "Dark"

        # affichage de la nouvelle fractale en mode "sombre"
        self.afficher_fractale()

        # Modification de toute l'interface (CustomTkinter, figure Matplotlib et barre d'outils)
        customtkinter.set_appearance_mode("Dark")

        self.figure.set_facecolor("#242424") # "242424" est le code couleur correspondant à l'interface sombre
        
        self.barre_outils.config(background="#242424")
        self.barre_outils._message_label.config(background="#242424")
        for bouton in self.barre_outils.winfo_children():
            bouton.config(background="#242424")
        self.barre_outils.update()






    # 3. Changer la qualité  (haute ou basse)
    def change_qualite_mode_event(self, nouvelle_qualite: str):
        '''Génère une nouvelle fractale avec la qualité sélectionnée (Haute ou Basse qualité)
           Entrée: valeur sélectionnée dans le menu de qualité'''
        
        if nouvelle_qualite == "Basse (plus rapide)":
            self.largeur, self.hauteur = 500, 500 # réduction des attributs correspondants pour réduire la qualité
            self.afficher_fractale() # nouvelle génération plus rapide

        else:
            self.largeur, self.hauteur = 2500, 2500 # augmentation des attributs correspondants pour augmenter la qualité
            self.afficher_fractale() # nouvelle génération en haute qualité






    # 4. Modifier la palette de couleur utilisée
    def update_couleur(self, event=None):
        '''Redessine la fractale avec les couleurs modifiées'''

        # modification de l'attribut correspondant avec la valeur sélectionée et application de la palette sur l'image
        self.couleur = self.couleur_var.get()
        self.rendu_image.set_cmap(self.couleur)

        # Rafraîchit le canvas pour voir les modifications de couleurs
        self.canvas.draw()






    # 5.a : Générer une nouvelle fractale de Mandelbrot avec ses nouveaux paramètres
    def update_mandelbrot(self, event=None):
        '''Appelle la méthode "afficher_fractale" et génère une nouvelle fractale de Mandelbrot avec son nombre d'itérations mis à jour'''

        # Récupère le nombre d'itérations et génère la fractale par l'appel de "afficher_fractale"
        self.type_fractale = self.type_fractale_var.get()
        if self.type_fractale == 'mandelbrot':
            self.max_iter = int(self.iter_slider.get())
            self.rendu_image = None
            self.barre_de_couleur.remove()
            self.afficher_fractale()        
    



    # 5.b : Générer une nouvelle fractale de Julia avec ses nouveaux paramètres
    def update_julia(self, event=None):
        '''Appelle la méthode "afficher_fractale" et génère un nouvel ensemble de Julia avec sa formule mise à jour'''

        # Récupère la formule de Julia et génère la fractale par l'appel de "afficher_fractale"
        self.type_fractale = self.type_fractale_var.get()
        if self.type_fractale == 'julia':
            self.formule_julia = self.formule_julia_var.get()
            self.afficher_fractale()






    # 6. Afficher la nouvelle fractale sur le plot Matplotlib
    def afficher_fractale(self):
        '''Sous-méthode qui appelle elle-même la sous-méthode "générer_fractale" pour générer puis afficher la fractale'''

        # Récupère dans la variable "image_fractale" la fractale générée avec "générer_fractale"
        image_fractale = self.generer_fractale()


        # Vérifie si l'image existe déjà
        if self.rendu_image:

            # Si l'image existe, met à jour les données de l'image avec image_fractale
            self.rendu_image.set_array(image_fractale)
            self.rendu_image.changed() # indique à Matplotlib que les données de l'image ont changé


        # Si l'image n'existe pas encore, crée une nouvelle image avec image_fractale
        else:

            self.rendu_image = self.axes.imshow(image_fractale, cmap=self.couleur, extent=(self.x_min, self.x_max, self.y_min, self.y_max)) # crée la nouvelle fractale avec sa couleur

            self.barre_de_couleur = self.figure.colorbar(self.rendu_image) # ajout de la barre de couleur


        self.axes.set_xticks([])  # Ne pas afficher les graduations sur l'axe des x
        self.axes.set_yticks([])  # Ne pas afficher les graduations sur l'axe des y


        self.axes.set_aspect('equal')  # Pour être sûr que les axes ont la même graduation


        plt.subplots_adjust(left=0.05, right=1.0) # Ajuste la position du plot Matplotlib


        # Rafraîchit le canvas pour refléter les changements
        self.canvas.draw()






    # 7. Génèrer une nouvelle fractale en fonction de tous ses paramètres : son type, son mode, et son nombre d'itérations (la couleur est seulement appliquée par la suite)
    def generer_fractale(self):
        '''Sous-méthode appelée par la sous-méthode "afficher_fractale. Elle génère une nouvelle fractale en appelant elle-même les méthodes (Mandelbrot ou Julia) correspondantes
           Sortie : l'image de la fractale'''
        
        # Crée une array NumPy vide ("np.zeros") pour stocker les profondeurs des pixels dans l'image fractale
        image = np.zeros((self.largeur, self.hauteur))

        # Parcourt tous les pixels de l'image
        for x in range(self.largeur):
            for y in range(self.hauteur):

                # Convertit les coordonnées du pixel en nombres complexes
                reel = self.x_min + (x / self.largeur) * (self.x_max - self.x_min)
                imaginaire = self.y_min + (y / self.hauteur) * (self.y_max - self.y_min)
                c = complex(reel, imaginaire)



                # ---- En fonction des paramètres sélectionnés, on appelle la fonction récursive correspondante

                # Si la fractale choisie est Mandelbrot et que le thème est "Dark"
                if self.type_fractale == 'mandelbrot' and self.mode == "Dark":
                    z = 0
                    n = 0
                    # On attribue à chaque coordonnée une valeur calculée 
                    image[x,y] = self.mandelbrot_recursive_sombre(c, z, n) # appel de la fonction récursive


                # Si la fractale choisie est Mandelbrot et que le thème est "Light"
                elif self.type_fractale == 'mandelbrot' and self.mode == "Light":
                    z = 0
                    n = 0
                    image[x,y] = self.mandelbrot_recursive_clair(c, z, n) # appel de la fonction récursive


                # Si la fractale choisie est Julia et que le thème est "Dark"
                elif self.type_fractale == 'julia' and self.mode == "Dark":
                    z = c
                    n = 0
                    image[x,y] = self.julia_recursive_sombre(c, z, n) # appel de la fonction récursive


                # Si la fractale choisie est Julia et que le thème est "Light"
                elif self.type_fractale == 'julia' and self.mode == "Light":
                    z = c
                    n = 0
                    image[x,y] = self.julia_recursive_clair(c, z, n) # appel de la fonction récursive



        # On renvoie l'image (qui va être stockée dans la variable "image_fractale" l. 494)
        return image






    # 7. CALCUL des différentes fractales possibles

    # ----- LOGIQUE DE CALCUL des Fractales Sombres -----
    # -> Si le nombre d'itérations atteint le maximum (n == self.max_iter), la profondeur du pixel est définie à 0 ; sinon la profondeur est définie à la valeur de n


    # 7.a : Calculer l'ensemble de Mandelbrot avec une apparence "sombre"
    def mandelbrot_recursive_sombre(self, c, z, n):
        '''Calcule et itère de manière récursive jusqu'à ce que la condition de convergence soit atteinte
        ou que le nombre maximal d'itérations (self.max_iter) soit atteint
        Sorties : valeurs attribuées aux éléments de l'array NumPy'''


        if abs(z) > 2 :
            return n
        elif n == self.max_iter:
            return 0 # C'est ce zéro là qui rend l'apparence "sombre" (car 0 correspond à "noir")
        else:
            return self.mandelbrot_recursive_sombre(c, z**2+c, n + 1) # appel récursif en incrémentant n de 1 et en effectuant la formule de Mandelbrot (z^2 + c)
        



    # 7.b : Calculer l'ensemble de Julia avec une apparence "sombre"
    def julia_recursive_sombre(self, c, z, n):
        '''Calcule et itère de manière récursive jusqu'à ce que la condition de convergence soit atteinte
        ou que le nombre maximal d'itérations (self.max_iter) soit atteint
        Sorties : valeurs attribuées aux éléments de l'array NumPy'''


        if abs(z) > 2 :
            return n
        elif n == self.max_iter:
            return 0 # C'est ce zéro là qui rend l'apparence "sombre" (car 0 correspond à "noir")
        else:
            return self.julia_recursive_sombre(c, z**2+complex(eval(self.formule_julia)), n + 1) # appel récursif en incrémentant n de 1 et en effectuant la formule de Julia (z^2 + formule de julia)






    # ----- LOGIQUE DE CALCUL des Fractales Claires -----
    # -> Si le nombre d'itérations atteint le maximum (n == self.max_iter) ou que la condition de convergence est atteinte, la profondeur est définie à la valeur de n


    # 7.c : Calculer l'ensemble de Mandelbrot avec une apparence "claire"
    def mandelbrot_recursive_clair(self, c, z, n):
        '''Calcule et itère de manière récursive jusqu'à ce que la condition de convergence soit atteinte
        ou que le nombre maximal d'itérations (self.max_iter) soit atteint
        Sorties : valeurs attribuées aux éléments de l'array NumPy'''


        if abs(z) > 2 or n == self.max_iter:
            return n # Puisque les 2 cas (convergence et nombre maximal d'itérations) sont confondus, la fractale va prendre l'apparence "claire" avec la valeur n
        else:
            return self.mandelbrot_recursive_clair(c, z**2+c, n + 1) # appel récursif en incrémentant n de 1 et en effectuant la formule de Mandelbrot (z^2 + c)
        



    # 7.d : Calculer l'ensemble de Julia avec une apparence "claire"
    def julia_recursive_clair(self, c, z, n):
        '''Calcule et itère de manière récursive jusqu'à ce que la condition de convergence soit atteinte
        ou que le nombre maximal d'itérations (self.max_iter) soit atteint
        Sorties : valeurs attribuées aux éléments de l'array NumPy'''


        if abs(z) > 2 or n == self.max_iter:
            return n # Puisque les 2 cas (convergence et nombre maximal d'itérations) sont confondus, la fractale va prendre l'apparence "claire" avec la valeur n
        else:
            return self.julia_recursive_clair(c, z**2+complex(eval(self.formule_julia)), n + 1) # appel récursif en incrémentant n de 1 et en effectuant la formule de Julia (z^2 + formule de julia)






# Lancement du programme en créant l'objet "app" de la Classe Générateur_fractale
if __name__ == "__main__":
    app = Generateur_fractale(customtkinter.CTk())